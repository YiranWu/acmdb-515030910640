package simpledb;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {

    private int[] cnt;
    private int buckets, min, max, num, ntup;

    /**
     * Create a new IntHistogram.
     * 
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     * 
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     * 
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't 
     * simply store every value that you see in a sorted list.
     * 
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
    	// some code goes here
        this.buckets = buckets;
        cnt = new int[buckets];
        ntup = 0;
        this.min = min;
        this.max = max;
        num = (int)Math.ceil((double)(max - min + 1) / buckets);
    }

    private int findPos(int v) {
        return (int)Math.floor((double)(v - min) / num);
    }

    private int findWidth(int pos) {
        return pos < buckets ? num : max - min - num * (buckets - 1);
    }

    private int findMax(int pos) {
        return pos < buckets ? num * (pos + 1) - 1 : max;
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    	// some code goes here
        cnt[findPos(v)]++;
        ntup++;
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     * 
     * For example, if "op" is "GREATER_THAN" and "v" is 5, 
     * return your estimate of the fraction of elements that are greater than 5.
     * 
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */
    public double estimateSelectivity(Predicate.Op op, int v) {
    	// some code goes here
        double ret=0;
        int pos = findPos(v), flag = 0;
        switch(op) {
            case EQUALS:
            case NOT_EQUALS:
                if(v<min || v>max) ret = 0;
                else ret = (double)cnt[pos] / findWidth(pos) / ntup;
                if(op == Predicate.Op.NOT_EQUALS) ret = 1 - ret;
                break;
            case GREATER_THAN:
            case GREATER_THAN_OR_EQ:
                flag = op == Predicate.Op.GREATER_THAN_OR_EQ ? 1 : 0;
                if(pos < 0) {
                    flag = 1;
                    pos = 0;
                    v = min;
                }
                //System.out.println("min = " + min + " max = " + max + " v = " + v + " ntup = " + ntup);

                if(pos >= buckets) ret = 0;
                else {
                    ret += (double)(findMax(pos) - (v - min) + flag) / findWidth(pos) * cnt[pos];
                    //System.out.println("ret = " + ret + " pos = " + pos + " cnt[" + pos + "] = " + cnt[pos]);
                    for(int i = pos + 1; i < buckets; i++) {
                        ret += cnt[i];
                        //System.out.println("ret = " + ret);
                    }
                    ret /= ntup;
                }
                //System.out.println(ret);
                break;
            case LESS_THAN:
            case LESS_THAN_OR_EQ:
                flag = op == Predicate.Op.LESS_THAN_OR_EQ ? 1 : 0;
                if(pos >= buckets) {
                    pos = buckets - 1;
                    flag = 1;
                    v = max;
                }
                if(pos < 0) ret = 0;
                else {
                    ret += (double)((v - min) - num * pos + flag) / findWidth(pos) * cnt[pos];
                    for(int i = 0; i < pos; i++)
                        ret += cnt[i];
                    ret /= ntup;
                }
                break;
        }
        return ret;
    }
    
    /**
     * @return
     *     the average selectivity of this histogram.
     *     
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        // some code goes here
        return 1.0;
    }
    
    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        // some code goes here
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < buckets; i++)
            builder.append((i * num) + "-" + findMax(i) + ":" + cnt[i] + "\n");
        return builder.toString();
    }
}

package simpledb;

import java.util.ArrayList;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private int gbfield,afield;
    private Type gbfieldtype;
    private Op what;
    private ArrayList<Field> field1;
    private ArrayList<Integer> count;

    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
        this.gbfield = gbfield;
        this.gbfieldtype = gbfieldtype;
        this.afield = afield;
        this.what = what;
        field1 = new ArrayList<Field>();
        count = new ArrayList<Integer>();
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
        int fieldNo=0;
        if(gbfield != NO_GROUPING) fieldNo = field1.indexOf(tup.getField(gbfield));
        if(fieldNo == -1) fieldNo = field1.size();
        if(fieldNo == count.size()) {
            if(gbfield != NO_GROUPING) field1.add(tup.getField(gbfield));
            count.add(1);
            return;
        }
        int c = count.get(fieldNo);
        switch(what) {
            case COUNT:
                count.set(fieldNo, c + 1);
                break;
        }
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        ArrayList<Tuple> ret = new ArrayList<Tuple>();
        TupleDesc desc = null;
        if(gbfield == NO_GROUPING) {
            Type[] type = new Type[1];
            type[0] = Type.INT_TYPE;
            desc = new TupleDesc(type);
            Tuple tuple = new Tuple(desc);
            int retval = 0;
            switch (what) {
                case COUNT:
                    retval = count.get(0);
                    break;
            }
            tuple.setField(0, new IntField(retval));
            ret.add(tuple);
        }
        else {
            Type[] type = new Type[2];
            type[0] = gbfieldtype;
            type[1] = Type.INT_TYPE;
            desc = new TupleDesc(type);
            Tuple tuple = null;
            for(int i = 0; i < count.size(); i++) {
                tuple = new Tuple(desc);
                int retval = 0;
                switch (what) {
                    case COUNT:
                        retval = count.get(i);
                        break;
                }
                tuple.setField(0, field1.get(i));
                tuple.setField(1, new IntField(retval));
                ret.add(tuple);
            }
        }
        return new TupleIterator(desc, ret);
    }

}

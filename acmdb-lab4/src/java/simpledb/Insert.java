package simpledb;

import java.io.IOException;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator {

    private static final long serialVersionUID = 1L;

    private TransactionId tid;
    private DbIterator child;
    private int tableId;
    private int cnt;
    private boolean flag;


    /**
     * Constructor.
     *
     * @param t
     *            The transaction running the insert.
     * @param child
     *            The child operator from which to read tuples to be inserted.
     * @param tableId
     *            The table in which to insert tuples.
     * @throws DbException
     *             if TupleDesc of child differs from table into which we are to
     *             insert.
     */
    public Insert(TransactionId t,DbIterator child, int tableId)
            throws DbException {
        // some code goes here
        if(child == null || !child.getTupleDesc().equals(Database.getCatalog().getTupleDesc(tableId)))
            throw new DbException("Insert: TupleDesc not match");
        this.tid =t;
        this.child = child;
        this.tableId = tableId;
        cnt = 0;
        flag = false;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        Type[] type = new Type[1];
        type[0] = Type.INT_TYPE;
        return new TupleDesc(type);
    }

    public void open() throws DbException, TransactionAbortedException {
        // some code goes here
        if(child == null) throw new DbException("Insert: child is null");
        super.open();
        child.open();
        cnt = 0;
        flag = false;
    }

    public void close() {
        // some code goes here
        child.close();
        super.close();
        cnt = 0;
        flag = false;
    }

    public void rewind() throws DbException, TransactionAbortedException {
        // some code goes here
        child.rewind();
        cnt = 0;
        flag = false;
    }

    /**
     * Inserts tuples read from child into the tableId specified by the
     * constructor. It returns a one field tuple containing the number of
     * inserted records. Inserts should be passed through BufferPool. An
     * instances of BufferPool is available via Database.getBufferPool(). Note
     * that insert DOES NOT need check to see if a particular tuple is a
     * duplicate before inserting it.
     *
     * @return A 1-field tuple containing the number of inserted records, or
     *         null if called more than once.
     * @see Database#getBufferPool
     * @see BufferPool#insertTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        // some code goes here
        if(child == null || flag) return null;
        while (child.hasNext()) {
            try {
                Database.getBufferPool().insertTuple(tid, tableId, child.next());
            } catch (IOException e) {
                e.printStackTrace();
            }
            cnt++;
        }
        Tuple ret = new Tuple(getTupleDesc());
        ret.setField(0, new IntField(cnt));
        flag = true;
        return ret;
    }

    @Override
    public DbIterator[] getChildren() {
        // some code goes here
        DbIterator[] children = new DbIterator[1];
        children[0] = child;
        return children;
    }

    @Override
    public void setChildren(DbIterator[] children) {
        // some code goes here
        child = children[0];
    }
}

package simpledb;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    private File file;
    private TupleDesc desc;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
        file = f;
        desc = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return desc;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        try {
            RandomAccessFile raf = new RandomAccessFile(this.file, "r");
            int offset = BufferPool.getPageSize() * pid.pageNumber();
            byte[] data = new byte[BufferPool.getPageSize()];
            assert(offset + BufferPool.getPageSize() <= file.length());
            raf.seek(offset);
            raf.readFully(data);
            raf.close();
            return new HeapPage((HeapPageId)pid, data);
        }
        catch(IOException e) {
            e.printStackTrace();
            throw new NoSuchElementException();
        }
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
        try {
            RandomAccessFile raf = new RandomAccessFile(this.file, "rw");
            raf.seek(page.getId().pageNumber() * BufferPool.getPageSize());
            raf.write(page.getPageData());
            raf.close();
        } catch(IOException e) {
            e.printStackTrace();
            throw new NoSuchElementException();
        }
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        return (int)Math.ceil(file.length() / BufferPool.getPageSize());
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
        if(!t.getTupleDesc().equals(desc)) throw new DbException("desc mismatch");
        int pageNo = 0;
        HeapPage page = null;
        for(pageNo=0; pageNo < numPages(); pageNo++) {
            page = (HeapPage) Database.getBufferPool().getPage(tid,
                                                               new HeapPageId(this.getId(), pageNo),
                                                               Permissions.READ_ONLY);
            if(page.getNumEmptySlots() > 0) break;
        }
        if(pageNo == numPages()) {
            page = new HeapPage(new HeapPageId(this.getId(), pageNo), HeapPage.createEmptyPageData());
            this.writePage(page);
            page = (HeapPage) Database.getBufferPool().getPage(tid,
                                                               new HeapPageId(this.getId(), pageNo),
                                                               Permissions.READ_WRITE);
        }
        page.insertTuple(t);
        ArrayList<Page> ret = new ArrayList<Page>();
        ret.add(page);
        return ret;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
        HeapPageId pageId = (HeapPageId)t.getRecordId().getPageId();
        if(pageId.getTableId() != getId())
            throw new DbException("HeapFile DeleteTuple failed");
        HeapPage page = (HeapPage) Database.getBufferPool().getPage(tid, pageId, Permissions.READ_WRITE);
        page.deleteTuple(t);
        ArrayList<Page> ret = new ArrayList<Page>();
        ret.add(page);
        return ret;
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
        return new HeapFileIterator(this, tid);
    }

    private class HeapFileIterator extends AbstractDbFileIterator {
        HeapFile heapFile;
        int pagePos;
        TransactionId tranId;
        Iterator<Tuple> tupleIter;

        public HeapFileIterator(HeapFile file, TransactionId id) {
            heapFile = file;
            tranId = id;
        }

        public void open() throws DbException, TransactionAbortedException {
            pagePos = -1;
        }

        @Override
        protected Tuple readNext() throws DbException, TransactionAbortedException {
            if (tupleIter != null && !tupleIter.hasNext()) tupleIter = null;

            while (tupleIter == null && pagePos < heapFile.numPages() - 1) {
                pagePos++;
                HeapPageId pageId = new HeapPageId(heapFile.getId(), pagePos);
                HeapPage page = (HeapPage) Database.getBufferPool().getPage(tranId,
                        pageId, Permissions.READ_ONLY);
                tupleIter = page.iterator();
                if (!tupleIter.hasNext())
                    tupleIter = null;
            }
            if (tupleIter == null)
                return null;
            return tupleIter.next();
        }

        public void rewind() throws DbException, TransactionAbortedException {
            close();
            open();
        }

        public void close() {
            super.close();
            tupleIter = null;
            pagePos = BufferPool.getPageSize() + 1;
        }
    }

}

